#![allow(dead_code)]
use std::path::PathBuf;
use tempfile::tempdir;

pub fn setup_temp_path() -> Result<PathBuf, Box<dyn std::error::Error>> {
    let dir = tempdir()?;
    let file_path = dir.path().join("turl.redb");
    Ok(file_path)
}
