mod common;

// Not able to add self as a dev dependency using cargo add --dev --path .
// use turl::appstate;
// Doesn't like super either, presumably this test crate isn't a child.
// use super::appstate;
// use crate::appstate::*;

// Disabled until we do anything serious here
#[ignore]
#[test]
fn my_test() {
    let my_dir = common::setup_temp_path();
    println!("{my_dir:?}");
    // Not recommended to actually run the app entirely.
    // let main_handle = std::thread::spawn(main);
    // ...even so, cargo_bin doesn't seem to be a thing.
    // let mut cmd = std::process::Command::cargo_bin("turl")?;
    // This seems to be the recommended approach but I can't import appstate module from here
    // let appstate = appstate::build(my_dir, false)?;
}
