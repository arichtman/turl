
use super::*;
use appstate::{State, StateTrait, SuccessfulSetResponse};
use axum::Json;
use mockall::predicate::*;
use rstest::{fixture, rstest};

use tempfile::tempdir;

const PRESET_ADDRESS: &str = "pre-set-address.internal";
const PRESET_KEY: &str = "4bc849d1";

#[fixture]
async fn common_state(#[default("false")] mutable: bool) -> State {
    let temp_dir = tempdir().unwrap();
    let temp_path = temp_dir.path().join("turl.redb");
    let common_state = appstate::State::new(temp_path, mutable).unwrap();
    let _ = common_state
        .set_with_address(PRESET_ADDRESS.to_string())
        .await
        .expect("Test fixture fucked up setting known data");
    common_state
}

// This seems wrong in testing, it's all too tightly coupled to implementation
fn make_set_response(key: String, address: String) -> Json<SuccessfulSetResponse> {
    Json(SuccessfulSetResponse::new(key, address))
}

#[rstest]
#[tokio::test]
async fn mutable<S: StateTrait>(
    #[future]
    #[with(true)]
    common_state: S,
) -> () {
    let my_state = common_state.await;
    // Re-set existing
    let set_response = my_state.set_with_address(PRESET_ADDRESS.to_string()).await;
    assert!(set_response.is_ok());
    // assert_eq!(Ok(PRESET_KEY.to_string()), set_response);
    // assert_eq!(
    //     (make_set_response(PRESET_KEY.into(), PRESET_ADDRESS.into())),
    //     set_response
    // );
    // Set new by address
    let set_response = my_state
        .set_with_address("new-address.internal".to_string())
        .await;
    assert!(set_response.is_ok());
    // assert_eq!(Ok("562066bf".to_string()), set_response);
    // Re-set new by address
    let set_response = my_state
        .set_with_address("new-address.internal".to_string())
        .await;
    assert!(set_response.is_ok());
    // assert_eq!(Ok("562066bf".to_string()), set_response);
}

#[rstest]
#[tokio::test]
async fn immutable<S: StateTrait>(#[future] common_state: S) -> () {
    let my_state = common_state.await;
    // Re-set existing
    let set_response = my_state.set_with_address(PRESET_ADDRESS.to_string()).await;
    assert!(set_response.is_ok());
}

#[rstest]
#[tokio::test]
async fn retrieval<S: StateTrait>(#[future] common_state: S) -> () {
    let my_state = common_state.await;
    // Retrieval by key
    let get_response = my_state.get_by_key(PRESET_KEY.to_string()).await;
    assert_eq!(Ok(Some(PRESET_ADDRESS.into())), get_response);
    // Retrieval by address
    let get_response = my_state.get_by_address(PRESET_ADDRESS).await;
    assert_eq!(Ok(Some(PRESET_ADDRESS.into())), get_response);
    // Non-existent address retrieval
    let get_response = my_state.get_by_address("foo").await;
    assert_eq!(Ok(None), get_response);
    // List all
    // // TODO: This test will flake if db throws any errors
    let get_response = my_state.get_all().await;
    assert_eq!(
        Ok(vec![(PRESET_KEY.into(), PRESET_ADDRESS.into())]),
        get_response
    );
}
