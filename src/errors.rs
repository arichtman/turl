use std::any::Any;

use axum::response::IntoResponse;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum InternalDataError {
    // NotFound,
    // AmbiguousResult,
    #[error("Address already taken, cannot replace '{existing_value}' with '{desired_value}'.")]
    AlreadyTaken {
        existing_value: String,
        desired_value: String,
    },
    // InvalidURL,
    // ReadLockUnavailable,
    // WriteLockUnavailable,
    #[error("Problem initializing database")]
    DatabaseInitialization,
    // TODO:: work out this enum constructor thing
    // DatabaseInitialization(#[from] redb::DatabaseError),
    #[error("IO problem")]
    IO(#[from] std::io::Error),
    #[error("Transaction problem")]
    DbTransaction(#[from] redb::TransactionError),
    #[error("Table problem")]
    DbTable(#[from] redb::TableError),
    #[error("Storage problem")]
    DbStorage(#[from] redb::StorageError),
    #[error("Commit problem")]
    DbCommit(#[from] redb::CommitError),
    #[error("Something went wrong... blame closed source")]
    Generic,
}

impl PartialEq for InternalDataError {
    // This is a naive implementation, but it's just to enable test condition for happy path
    // TODO: if we start testing unhappy paths, flesh this out to compare internal error types
    fn eq(&self, other: &Self) -> bool {
        self.type_id() == other.type_id()
    }
}

impl IntoResponse for InternalDataError {
    fn into_response(self) -> axum::response::Response {
        // Yea yea this stinks, it's whatever temporary
        let body = axum::body::Body::empty();
        let mut reply = axum::response::Response::new(body);
        *reply.status_mut() = axum::http::StatusCode::INTERNAL_SERVER_ERROR;
        // let mut code = reply.status_mut();
        // let *code = axum::http::StatusCode;
        unreachable!("not propagating errors... yet");
        reply
        // todo!()
    }
}
