use axum::response::IntoResponse;
use axum::{async_trait, Json};

use std::sync::Arc;

use tracing::debug;
use tracing::trace;

use crate::errors::InternalDataError;

#[cfg(test)]
use mockall::automock;

use serde::Serialize;

use redb::TableHandle;

#[derive(Debug)]
pub(crate) struct State {
    url_mappings: redb::Database,
    mutable: bool,
}

const TABLE: redb::TableDefinition<String, String> = redb::TableDefinition::new("turl");

impl State {
    pub fn new(
        db_destination_path: std::path::PathBuf,
        allow_overwrites: bool,
    ) -> Result<State, Box<dyn std::error::Error>> {
        trace!("State::new");
        let Ok(db) = redb::Database::create(db_destination_path) else {
            return Err(Box::new(InternalDataError::DatabaseInitialization));
        };
        // TODO: Keep this? It's more for debugging at this stage... Allow different namespaces using tables?
        let tables: Vec<String> = db
            .begin_read()?
            .list_tables()?
            // TODO: This is an odd way to solve it. Wonder if there's a more idiomatic way...
            .map(|v| String::from(v.name()))
            .collect();
        // Initialize table if not exist requires write transaction
        // Ref: https://github.com/cberner/redb/issues/920
        let write_tx = db.begin_write()?;
        {
            let table = write_tx.open_table(TABLE)?;
        }
        write_tx.commit()?;
        Ok(State {
            url_mappings: db,
            mutable: allow_overwrites,
        })
    }
    // TODO: Think about if this should be a static method or instance one
    fn get_key_from_address(address: &str) -> String {
        sha256::digest(address)
            .chars()
            .take(8)
            .fold("".to_string(), |a, v| format!("{a}{v}"))
    }
}

// TODO: Think about if this abstraction is adding anything
pub(crate) fn build(
    db_destination_path: std::path::PathBuf,
    mutable: bool,
) -> Result<DynStateTrait, Box<dyn std::error::Error>> {
    let state = State::new(db_destination_path, mutable)?;
    Ok(Arc::new(state) as DynStateTrait)
}

// Use a trait to decouple data access layer
#[cfg_attr(test, automock)]
#[async_trait]
pub(crate) trait StateTrait {
    async fn get_all(&self) -> Result<Vec<(String, String)>, InternalDataError>;
    async fn get_by_address(&self, name: &str) -> Result<Option<String>, InternalDataError>;
    async fn set_with_address(
        &self,
        address: String,
    ) -> Result<SetResponse, Box<dyn std::error::Error>>;
    async fn get_by_key(&self, key: String) -> Result<Option<String>, InternalDataError>;
}

#[cfg_attr(test, automock)]
#[async_trait]
impl StateTrait for State {
    async fn get_all(&self) -> Result<Vec<(String, String)>, InternalDataError> {
        trace!("get_all");
        let read_txn = self.url_mappings.begin_read()?;
        debug!("Got read transaction: {read_txn:?}");
        let table = read_txn.open_table(TABLE)?;
        debug!("Opened table: {table:?}");
        let maybe_existing_entries = table.range("\u{0000}".to_string().."\u{FFFF}".to_string())?;
        // TODO: Fix this data structure so it's a map
        let transformed_pairs: Vec<(String, String)> = maybe_existing_entries
            .into_iter()
            // TODO: this is hacky as fuuuuck
            .map(|v| v.unwrap())
            .map(|ag| (ag.0.value(), ag.1.value()))
            .collect();
        Ok(transformed_pairs)
    }
    async fn get_by_key(&self, key: String) -> Result<Option<String>, InternalDataError> {
        debug!(key);
        // TODO: Think about whether to put this in a local scope so it drops quickly
        let read_txn = self.url_mappings.begin_read()?;
        debug!("Got read transaction: {read_txn:?}");
        let table = read_txn.open_table(TABLE)?;
        debug!("Opened table: {table:?}");
        let maybe_existing_entries = table.get(key)?;
        // Need to convert the Some variant AccessGuard into String by calling instance method
        Ok(match maybe_existing_entries {
            None => None,
            Some(v) => Some(v.value()),
        })
    }
    async fn get_by_address(&self, address: &str) -> Result<Option<String>, InternalDataError> {
        trace!("get_by_address: {address}");
        let slug = State::get_key_from_address(address);
        self.get_by_key(slug).await
    }
    // TODO: think about the address API being &str vs String, efficiency and consistency
    async fn set_with_address(
        &self,
        address: String,
    ) -> Result<SetResponse, Box<dyn std::error::Error>> {
        trace!("set_with_address: {address}");
        let slug = State::get_key_from_address(&address);
        // TODO: Think about whether to hold a long running write transaction and just commit frequently
        // Thought: if we use long-running properties for transactions we can encode the mutability in the type system and let redb enforce RO
        let write_txn = self.url_mappings.begin_write()?;
        {
            let mut table = write_txn.open_table(TABLE)?;
            let maybe_old_value = table.insert(&slug, &address)?;
        }
        write_txn.commit()?;
        Ok(SetResponse::Successful { key: slug, address })
    }
}

#[derive(Serialize, Debug)]
pub(crate) enum SetResponse {
    Successful {
        key: String,
        address: String,
    },
    Taken {
        existing_value: String,
        desired_value: String,
    },
}

#[derive(Serialize, Debug)]
pub(crate) struct SuccessfulSetResponse {
    key: String,
    address: String,
}

impl SuccessfulSetResponse {
    pub(crate) fn new(key: String, address: String) -> Self {
        SuccessfulSetResponse { key, address }
    }
}

// This kiiinda makes sense? The pure response type doesn't belong as JSON,
//   neither should the handler function do the translation.
impl IntoResponse for SetResponse {
    fn into_response(self) -> axum::response::Response {
        Json(self).into_response()
    }
}
// Trait object. idk, it's cursed arcane incantation
pub type DynStateTrait = Arc<dyn StateTrait + Send + Sync>;
