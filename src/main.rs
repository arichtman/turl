// TODO: Remove for production
#![allow(dead_code, unused_imports, unused_variables, unreachable_code)]

use std::net::Ipv6Addr;

use axum_otel_metrics::HttpMetricsLayerBuilder;

use tokio::net::TcpListener;

mod appstate;
mod errors;
mod routes;
use tracing::debug;

use clap::{arg, command, Parser};

// TODO: Update env use when issue is resolved https://github.com/clap-rs/clap/issues/3221
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None, )]
struct Cli {
    #[arg(
        short,
        long,
        env = "TURL_FILE_PATH",
        // Ref: https://stackoverflow.com/a/76341487
        // Note: This will have compatibility issues with non-*nix systems.
        // If we were being really nice we'd use current execution dur and push() construction
        default_value = std::path::PathBuf::from("./turl.redb").into_os_string(),
        long_help = "File path to store data in. Can be an existing data store."
    )]
    file_path: std::path::PathBuf,
    #[arg(short = 'o', long, env = "TURL_MUTABLE", default_value_t = false)]
    mutable: bool,
    #[arg(short = 'p', long, env = "TURL_PORT", default_value_t = 1443)]
    port: u16,
    /// Increments logging verbosity.
    #[arg(short, long, action = clap::ArgAction::Count, env = "TURL_VERBOSE", long_help = "Optional. May be applied as arguments up to 4 times. Environment variable takes integer.")]
    verbose: u8,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let cli_opts = Cli::parse();
    use tracing::Level;
    let max_log_level = match cli_opts.verbose {
        0 => Level::ERROR,
        1 => Level::WARN,
        2 => Level::INFO,
        3 => Level::DEBUG,
        4.. => Level::TRACE,
    };
    let subscriber = tracing_subscriber::FmtSubscriber::builder()
        .with_max_level(max_log_level)
        .finish();
    tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");
    debug!("Options: {cli_opts:?}");
    debug!("Max log level: {max_log_level}");
    let listener = TcpListener::bind((Ipv6Addr::UNSPECIFIED, cli_opts.port)).await?;
    let state = appstate::build(cli_opts.file_path, cli_opts.mutable)?;
    let metrics = HttpMetricsLayerBuilder::new()
        .with_service_name(env!("CARGO_PKG_NAME").into())
        .with_service_version(env!("CARGO_PKG_VERSION").into())
        .with_prefix(env!("CARGO_PKG_NAME").into())
        .build();
    // TODO: Unclear why a trailing .into() failed on conversion when I have a generic error conversion
    let my_hack = axum::serve(
        // axum::serve(
        listener,
        routes::app(state).merge(metrics.routes()).layer(metrics),
    )
    .await?;
    Ok(my_hack)
}

#[cfg(test)]
mod tests;
