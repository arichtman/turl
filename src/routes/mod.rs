use axum::Router;

use crate::appstate::DynStateTrait;


mod root;
mod set;

pub fn app(state: DynStateTrait) -> Router {
    Router::new()
        .merge(root::app())
        .nest("/set", set::app())
        .with_state(state)
}
