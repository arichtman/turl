use axum::extract::Path;
use axum::extract::State;

use axum::response::IntoResponse;
use axum::routing::get;

use axum::{debug_handler, Router};
use reqwest::Url;
use tracing::debug;

use crate::appstate::DynStateTrait;
use crate::errors::InternalDataError;

pub fn app() -> Router<DynStateTrait> {
    Router::new().route("/:url", get(set).put(set).post(set))
}
#[debug_handler(state = DynStateTrait)]
pub async fn set(
    Path(path): Path<String>,
    State(state): State<DynStateTrait>,
) -> impl IntoResponse {
    debug!("Hit set path: {path}");
    // This is a bit odd. We're only using Url to check parsing, not for type safety
    // ...and discarding the result, not even passing it on.
    // but to switch the table definition to use Url requires a wrapper struct so
    // ...we can implement redb::Value, which seems like a lot of hassle.
    // Alternatively we'll need to convert back to string at all the table write call sites,
    // ...which is a  lot of processing for no actual utility.
    // I suppose they compile down but it's still more complex for no gain
    // Actually, should the parsing check be here at all or rather in routing or
    // ...a general entrance call to appstate?
    if let Ok(_) = Url::parse(path.as_str()) {
        // TODO: See about propagating Redb errors OR doing the conversion in AppState
        state
            .set_with_address(path)
            .await
            .map_err(|_| InternalDataError::Generic)
    } else {
        todo!()
    };
}
