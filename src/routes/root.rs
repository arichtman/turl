use axum::extract::State;

use axum::extract::Path;
use axum::http::StatusCode;
use axum::response::IntoResponse;
use axum::response::Redirect;
use axum::routing::get;

use axum::{debug_handler, Json, Router};

use tracing::trace;

use crate::appstate::DynStateTrait;

pub fn app() -> Router<DynStateTrait> {
    Router::new()
        .route("/", get(root_get))
        .route("/:path", get(root_get_specific))
}

#[debug_handler(state = DynStateTrait)]
pub async fn root_get_specific(
    Path(path): Path<String>,
    State(state): State<DynStateTrait>,
) -> impl IntoResponse {
    trace!("root_get_specific: {path}");

    // Annoying that we can't just ?-propagate the error even with an imp IntoResponse
    // TODO: hwat is going on calling into_response on all these...
    //   also... should this just be a big ol' match?
    let Ok(response) = state.get_by_key(path).await else {
        return StatusCode::INTERNAL_SERVER_ERROR.into_response();
    };
    let Some(destination) = response else {
        return StatusCode::NOT_FOUND.into_response();
    };
    Redirect::temporary(destination.as_str()).into_response()
}

#[debug_handler(state = DynStateTrait)]
pub async fn root_get(State(state): State<DynStateTrait>) -> impl IntoResponse {
    trace!("root_get");
    let Ok(urls) = state.get_all().await else {
        return Err(StatusCode::INTERNAL_SERVER_ERROR);
    };
    // TODO: Ensure map is returned in JSON
    Ok(Json(urls))
}
